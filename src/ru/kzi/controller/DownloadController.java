package ru.kzi.controller;

import ru.kzi.downloader.ImageListDownLoader;
import ru.kzi.downloader.MusicListDownloader;
import ru.kzi.interfaces.ILaunch;
import ru.kzi.interfaces.IMessage;
import ru.kzi.interfaces.IPaths;
import ru.kzi.scout.MusicScout;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс, экземпляр которого будет взаимодейстовать с пользователем, искать список
 * композиций (по тому ключу, который введет пользователь), и ставить композиции на
 * скачивание (предварительно дав возможность выбора композиций на скачивание
 * пользователю)
 *
 * @author KZI 17ИТ17
 */
public class DownloadController implements IPaths, IMessage, ILaunch {
    private static Scanner scanner = new Scanner(System.in);

    /**
     * Переопределяемый метод launch, необходимый для запуска экземпляра
     * данного класса на выполнение своей задачи
     */
    @Override
    public void launch() {
        try {
            System.out.println(INFO_MESSAGE_FOR_SEARCH);
            String answer = scanner.nextLine();
            MusicScout musicScout = new MusicScout(answer);
            musicScout.explore();
            if (!musicScout.getCompositions().isEmpty()) {
                printMusicList(musicScout.getCompositions());
                System.out.println(INFO_MESSAGE_FOR_INTERACTION_WITH_LIST);
                answer = enterData();
                startInteractionWithList(answer, musicScout);
            } else {
                System.out.println(NOTHING_FOUND);
            }

        } catch (IOException e) {
            System.out.println(FAIL_TO_CONNECTION);
        }
    }

    /**
     * Метод, который выводит на экран пользователя список
     * композиций и его индекс.
     *
     * @param compositions список названий композиций
     */
    private static void printMusicList(ArrayList<String> compositions) {
        for (int i = 0; i < compositions.size(); i++) {
            System.out.printf("%d %s%n", (i + 1), compositions.get(i));
        }
    }


    /**
     * Метод, который позволяет пользователю ввести необходимые данные
     * для последующей работы программы. Если данные некорректные, то
     * предоставляется возможность вводить их до тех пор, пока
     * они (данные) не пройдут проверку
     *
     * @return введенные данные, которая программа посчитала корректными
     */
    private static String enterData() {
        String temp = scanner.nextLine();
        while (!isCorrectAnswer(temp.trim())) {
            System.out.println(INVALID_DATA);
            temp = scanner.nextLine();
        }
        return temp.trim();
    }

    /**
     * Метод, проверяющий данные, введенные пользователем на корректность.
     * Проверка проходит по следующим критериям:
     * а) введенные данные являются командой на пролистывание списка композиций
     * б) введенные данные являются списком композиций, которые пользователь хочет скачать
     * в) введенные данные являются диапазоном индексов композиций.
     *
     * @param answer введенные пользователем данные
     * @return true если один из вышеперечисленных критериев оказался верным
     */
    private static boolean isCorrectAnswer(String answer) {
        boolean boolOne = isIndexList(answer);
        boolean boolTwo = isIndexRange(answer);
        boolean boolThree = isSwipeProcess(answer);
        return boolOne || boolTwo || boolThree;
    }

    /**
     * Метод, который проверяет данные, введенные пользователеи на наличие
     * команд пролистывания списка композиций.
     *
     * @param answer данные, введенные пользователем
     * @return true если данные являются командой на пролистывание списка
     */
    private static boolean isSwipeProcess(String answer) {
        return answer.equalsIgnoreCase("далее") ||
                answer.equalsIgnoreCase("назад");
    }

    /**
     * Метод, который проверяет данные, введенные пользователем на
     * следующий критерий:
     * введенные данные являются диапазоном индексов композиций
     *
     * @param answer данные, введенные пользователем
     * @return true если данные являются диапазоном индексов композиций
     */
    private static boolean isIndexRange(String answer) {
        return answer.matches("\\d{1,2}-\\d{1,2}");
    }

    /**
     * Метод, который проверяет данные, введенные пользователем на
     * следующий критерий:
     * введенные данные являются списком композиций, которые пользователь хочет скачать
     *
     * @param answer данные, введенные пользователем
     * @return true если данные являются списком композиций на скачивание
     */
    private static boolean isIndexList(String answer) {
        return answer.matches("(\\d{1,2},?)+");
    }

    /**
     * Метод, который начинает взаимодействие пользователя со списком композиций.
     * Данная функция включает в себя возможность перелистывать список композиций,
     * скачивать список песен или определенный диапазон.
     *
     * @param answer     действие, которое выбрал пользователь
     * @param musicScout объект, цель которого искать песни по ключу, который
     *                   ввел пользователь. Содержит в себе список песен и URL,
     *                   где содержатся композиции
     * @throws IOException исключение, выбрасываемое при ошибке с вводом/выводом
     */
    private static void startInteractionWithList(String answer, MusicScout musicScout)
            throws IOException {
        while (isSwipeProcess(answer)) {
            startPageSwipe(answer, musicScout);
            answer = enterData();
        }
        final ArrayList<String> musicNames = musicScout.getCompositions();
        int[] indexes;
        if (isIndexList(answer)) {
            indexes = getIndexes(answer);
            new MusicListDownloader(new URL(musicScout.getUrl()), musicNames,
                    indexes).download();
            new ImageListDownLoader(new URL(musicScout.getUrl()), musicNames,
                    indexes).download();
        } else if (isIndexRange(answer)) {
            indexes = formIndexes(answer);
            new MusicListDownloader(new URL(musicScout.getUrl()), musicNames,
                    indexes).download();
            new ImageListDownLoader(new URL(musicScout.getUrl()), musicNames,
                    indexes).download();

        }
    }


    /**
     * Метод, который начинает пролистывание списка композиций
     * в зависимости от полученной команды
     *
     * @param answer     введенная пользователем команда
     * @param musicScout объект, цель которого искать песни по ключу, который
     *                   ввел пользователь. Содержит в себе список песен и URL,
     *                   где содержатся композиции
     * @throws IOException исключение, выбрасываемое при ошибке с вводом/выводом
     */
    private static void startPageSwipe(String answer, MusicScout musicScout) throws IOException {
        if (answer.equalsIgnoreCase("далее")) {
            swipeToNext(musicScout);
        } else if (answer.equalsIgnoreCase("назад")) {
            swipeToBack(musicScout);
        }
    }

    /**
     * Метод, который пролистывает список композиций на следующую страницу
     *
     * @param musicScout объект, цель которого искать песни по ключу, который
     *                   ввел пользователь. Содержит в себе список песен и URL,
     *                   где содержатся композиции
     * @throws IOException исключение, выбрасываемое при ошибке с вводом/выводом
     */
    private static void swipeToNext(MusicScout musicScout) throws IOException {
        musicScout.nextPage();
        musicScout.explore();
        printMusicList(musicScout.getCompositions());
    }

    /**
     * Метод, который пролистывает список композиций на предыдущую страницу
     *
     * @param musicScout объект, цель которого искать песни по ключу, который
     *                   ввел пользователь. Содержит в себе список песен и URL,
     *                   где содержатся композиции
     * @throws IOException исключение, выбрасываемое при ошибке с вводом/выводом
     */
    private static void swipeToBack(MusicScout musicScout) throws IOException {
        musicScout.previousPage();
        musicScout.explore();
        printMusicList(musicScout.getCompositions());
    }

    /**
     * Метод, который извлекает индексы композиций и формирует
     * массив типа int, содержащий индексы скачиваемых композиций
     *
     * @param data список индексов композиций, введенный пользователем
     * @return массив типа int, содержащий индексы песен для скачивания
     */
    private static int[] getIndexes(String data) {
        String[] temp = data.split("\\W");
        int[] indexes = new int[temp.length];
        for (int i = 0; i < indexes.length; i++) {
            indexes[i] = Integer.valueOf(temp[i]) - 1;
        }
        return indexes;
    }

    /**
     * Метод, который формирует индексы скачиваемых композиций
     * по диапазону. Предусмотрено формирование индексов при случае, если
     * диапазон указан справа на лево.
     *
     * @param data диапазон песен
     * @return массив типа int, содержащий индексы композиций на скачивание
     */
    private static int[] formIndexes(String data) {
        String[] temp = data.split("-");
        int firstElement = Integer.valueOf(temp[0]);
        int secondElement = Integer.valueOf(temp[1]);
        int index;
        int[] indexes;
        if (firstElement > secondElement) {
            indexes = new int[firstElement - secondElement + 1];
            index = secondElement - 1;
        } else {
            indexes = new int[secondElement - firstElement + 1];
            index = firstElement - 1;
        }
        for (int i = 0; i < indexes.length; i++) {
            indexes[i] = index;
            index++;
        }
        return indexes;
    }

}
