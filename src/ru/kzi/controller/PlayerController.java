package ru.kzi.controller;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import ru.kzi.interfaces.ILaunch;
import ru.kzi.interfaces.IMessage;
import ru.kzi.interfaces.IPaths;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * Класс, экземпляр которого играет роль музыкального плеера
 *
 * @author KZI 17ИТ17
 */
public class PlayerController implements IMessage, IPaths, ILaunch {
    private static Scanner scanner = new Scanner(System.in);

    /***
     * Переопределяемый метод, который предназначен для запуска
     * музыкального плеера
     */
    @Override
    public void launch() {
        File folder = new File(DOWNLOAD_MUSIC_PATH);
        String[] music = folder.list((dir, name) -> name.endsWith(".mp3"));
        if (music != null) {
            printMusicList(music);
            System.out.println(INFO_MESSAGE_FOR_CHOICE_OF_MUSIC);
            int index = enterIndex();
            try (FileInputStream inputStream = new
                    FileInputStream(folder.getAbsoluteFile()
                    + "\\" + music[index - 1])) {
                Player player = new Player(inputStream);
                player.play();
            } catch (IOException e) {
                System.out.println(FILE_NOT_FOUND);
            } catch (JavaLayerException e) {
                System.out.println(FAIL_PLAY_MUSIC);
            } catch (IndexOutOfBoundsException e) {
                System.out.printf(INDEX_OUT_OF_BOUNDS, index);
            }
        } else {
            System.out.println(NOTHING_FOUND);
        }
    }

    /**
     * Метод, предназначенный для ввода данных (индекс песни для воспроизведения).
     * При вводе некорректных данных метод позволяет вновь ввести индекс
     * песни, проинформировав пользователя об его ошибке
     *
     * @return индекс песни для воспроизведения
     */
    private int enterIndex() {
        String temp;
        while (!(temp = scanner.nextLine()).trim()
                .matches("\\d{1,7}")) {
            System.out.println(INVALID_DATA);
        }
        return Integer.parseInt(temp.trim());
    }

    /**
     * Метод, предназначенный для вывода на экран
     * список имеющихся композиций и их индексы.
     *
     * @param music массив, содержащий названия композиций
     */
    private static void printMusicList(String[] music) {
        for (int i = 0; i < music.length; i++) {
            System.out.printf("%d %s%n", (i + 1), music[i]);
        }
    }
}