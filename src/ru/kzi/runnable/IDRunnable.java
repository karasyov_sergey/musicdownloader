package ru.kzi.runnable;

import ru.kzi.downloader.ImageDownloader;

/**
 * Класс, предназначенный для запуска скачивания изображений
 *
 * @author KZI 17ИТ17
 */

public class IDRunnable implements Runnable {
    private ImageDownloader imageDownloader;

    public IDRunnable(ImageDownloader imageDownloader) {
        this.imageDownloader = imageDownloader;
    }

    @Override
    public void run() {
        imageDownloader.download();
    }
}
