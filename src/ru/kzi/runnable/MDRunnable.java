package ru.kzi.runnable;

import ru.kzi.downloader.MusicDownloader;

/**
 * Класс, предназначенный для запуска скачивания композиций
 *
 * @author KZI 17ИТ17
 */

@SuppressWarnings("unused")
public class MDRunnable implements Runnable {
    private MusicDownloader musicDownloader;

    public MDRunnable(MusicDownloader musicDownloader) {
        this.musicDownloader = musicDownloader;
    }

    @Override
    public void run() {
        musicDownloader.download();
    }
}
