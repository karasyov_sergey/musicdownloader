package ru.kzi.interfaces;

/**
 * Класс вида интерфейс, содержащий метод launch
 *
 * @author KZI 17ИТ17
 */
public interface ILaunch {
    void launch();
}
