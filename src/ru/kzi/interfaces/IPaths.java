package ru.kzi.interfaces;

/**
 * Класс вида интерфейс, содержащий строковые константы, в
 * которых прописаны все необходимые пути для работы программы
 *
 * @author KZI 17ИТ17
 */
public interface IPaths {
    String MAIN_URL = "https://zenmusic.me";
    String DOWNLOAD_MUSIC_PATH = "src/ru/kzi/download/music/";
    String DOWNLOAD_IMAGE_PATH = "src/ru/kzi/download/image/";
}
