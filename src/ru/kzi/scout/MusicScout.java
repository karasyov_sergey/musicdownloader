package ru.kzi.scout;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.kzi.interfaces.IPaths;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Класс, экземпляр которого ищет список песен на сайте
 *
 * @author KZI 17ИТ17
 */

public class MusicScout implements IPaths {
    private ArrayList<String> compositions;
    private String url;

    public MusicScout(String searchCode) {
        this.compositions = new ArrayList<>();
        this.url = MAIN_URL + "/search?q=" + searchCode;
    }

    /**
     * Метод, который исследует веб ресурс и заполняет списочный массив (compositions) композициями.
     *
     * @throws IOException исключение, выбрасываемое при ошибке при выводе, вводе
     */

    public void explore() throws IOException {
        Document htmlDoc = Jsoup.connect(url).get();
        Elements mainTitles = htmlDoc.getElementsByClass("playlist-item-title");
        Elements subTitles = htmlDoc.getElementsByClass("playlist-item-subtitle");
        for (int i = 0; i < mainTitles.size(); i++) {
            compositions.add(subTitles.get(i).text() + " - " + mainTitles.get(i).text());
        }
    }

    /**
     * Метод, который организует переход на следующую страницу
     *
     * @throws IOException исключение, выбрасываемое при ошибке
     */

    public void nextPage() throws IOException {
        compositions.clear();
        Document htmlDoc = Jsoup.connect(url).get();
        Elements pages = htmlDoc.getElementsByClass("paggination__link pagination__btn");
        for (int i = pages.size() - 1; i >= 0; i--) {
            if (pages.get(i).text().equals(">")) {
                url = MAIN_URL + pages.get(i).attr("href");
                break;
            }
        }
    }

    /**
     * Метод, который организует переход на предыдущую страницу
     *
     * @throws IOException исключение, выбрасываемое при ошибке
     */

    public void previousPage() throws IOException {
        compositions.clear();
        Document htmlDoc = Jsoup.connect(url).get();
        Elements pages = htmlDoc.getElementsByClass(
                "paggination__link pagination__btn");
        for (int i = pages.size() - 1; i >= 0; i--) {
            if (pages.get(i).text().equals("<") || pages.get(i).text().equals("1")) {
                url = MAIN_URL + pages.get(i).attr("href");
                break;
            }
        }
    }

    /**
     * Метод, который возвращает список композиций
     *
     * @return список композиций
     */

    public ArrayList<String> getCompositions() {
        return compositions;
    }

    /**
     * Метод, который возращает указатель ресурса
     *
     * @return url
     */

    public String getUrl() {
        return url;
    }
}
