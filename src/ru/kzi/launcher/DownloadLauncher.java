package ru.kzi.launcher;


import ru.kzi.controller.DownloadController;

/**
 * Класс, который запускает на выполнение скачивателя музыки
 *
 * @author KZI 17ИТ17
 */
public class DownloadLauncher {

    public static void main(String[] args) {
        new DownloadController().launch();
    }
}