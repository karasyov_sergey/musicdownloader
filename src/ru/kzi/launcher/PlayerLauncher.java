package ru.kzi.launcher;

import ru.kzi.controller.PlayerController;

/**
 * Класс, который запускает на выполнение плеер, для проигрывания скачанных композиций
 *
 * @author KZI 17ИТ17
 */
public class PlayerLauncher {
    public static void main(String[] args) {
        new PlayerController().launch();
    }
}
