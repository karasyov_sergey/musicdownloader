package ru.kzi.downloader;

import ru.kzi.interfaces.IMessage;
import ru.kzi.interfaces.IPaths;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Класс экземпляр которого скачивает музыку
 *
 * @author KZI 17ИТ17
 */
@SuppressWarnings("unused")
public class MusicDownloader extends Downloader implements IPaths, IMessage {
    private String fileName;

    MusicDownloader(URL url, String fileName) {
        super(url);
        this.fileName = fileName;
    }

    /**
     * Метод, который скачивает файл в формате(.mp3)
     **/
    @Override
    public void download() {
        try (FileOutputStream stream = new FileOutputStream(DOWNLOAD_MUSIC_PATH +
                fileName + ".mp3");
             ReadableByteChannel byteChannel = Channels.newChannel(getUrl().openStream())
        ) {
            System.out.printf(DOWNLOAD_START, getFileName());
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            System.out.printf(DOWNLOAD_COMPLETE, getFileName());
        } catch (IOException e) {
            System.out.println(FAIL_TO_CONNECTION);
        }
    }

    /**
     * Метод, который получает имя файла
     *
     * @return имя файла
     */
    private String getFileName() {
        return fileName;
    }
}
