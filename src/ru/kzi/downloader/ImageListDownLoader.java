package ru.kzi.downloader;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.kzi.interfaces.IPaths;
import ru.kzi.runnable.IDRunnable;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Класс экземпляр, скачивает список изображений
 *
 * @author KZI 17ИТ17
 */
public class ImageListDownLoader extends Downloader implements IPaths {
    private ArrayList<String> fileNames;
    private int[] indexesForDownload;

    public ImageListDownLoader(URL url, ArrayList<String> fileNames,
                               int[] indexesForDownload) {
        super(url);
        this.fileNames = fileNames;
        this.indexesForDownload = indexesForDownload;
    }

    /**
     * Переопределяемый метод, который скачивает список изображений
     */
    @Override
    public void download() {
        try {
            Document htmlDoc = Jsoup.connect(getUrl().toString()).get();
            Elements elements = htmlDoc.getElementsByAttribute("data-img");
            URL url;
            for (Integer integer : indexesForDownload) {
                try {
                    url = getUrlForDownload(elements.get(integer));
                    new Thread(new IDRunnable(new ImageDownloader(url,
                            fileNames.get(integer)))).start();
                } catch (IndexOutOfBoundsException ignored) {

                }
            }
        } catch (IOException ignored) {

        }
    }

    /**
     * Метод, который ищет ссылку на скачивние в html документе
     *
     * @param el В нём находится html тег
     * @return возвращает ссылку
     * @throws MalformedURLException исключение выбрасываемое при ошибке с URL адресом
     */
    private URL getUrlForDownload(Element el) throws MalformedURLException {
        return new URL(MAIN_URL + el.attr("data-img"));
    }
}


