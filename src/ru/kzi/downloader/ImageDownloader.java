package ru.kzi.downloader;

import ru.kzi.interfaces.IPaths;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Класс экземпляр которого скачивает картинку
 *
 * @author KZI 17ИТ17
 */
@SuppressWarnings("unused")
public class ImageDownloader extends Downloader implements IPaths {
    private String fileName;

    ImageDownloader(URL url, String fileName) {
        super(url);
        this.fileName = fileName;
    }

    /**
     * Метод, который скачивает картинку в формате(.jpg)
     **/
    @Override
    public void download() {
        try (FileOutputStream stream = new FileOutputStream(DOWNLOAD_IMAGE_PATH +
                getFileName() + ".jpg");
             ReadableByteChannel byteChannel = Channels.newChannel(getUrl().openStream())
        ) {
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        } catch (IOException ignored) {
        }
    }

    /**
     * Метод, который получает имя файла
     *
     * @return имя файла
     */
    private String getFileName() {
        return fileName;
    }
}