package ru.kzi.downloader;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.kzi.interfaces.IMessage;
import ru.kzi.interfaces.IPaths;
import ru.kzi.runnable.MDRunnable;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Класс наследник, который скачивает список музыки
 *
 * @author KZI17ИТ17
 */
public class MusicListDownloader extends Downloader implements IPaths, IMessage {
    private ArrayList<String> fileNames;
    private int[] indexesForDownload;

    public MusicListDownloader(URL url, ArrayList<String> fileNames,
                               int[] indexesForDownload) {
        super(url);
        this.fileNames = fileNames;
        this.indexesForDownload = indexesForDownload;
    }

    /**
     * Метод, который скачивает список музыки
     */
    @Override
    public void download() {
        try {
            Document htmlDoc = Jsoup.connect(getUrl().toString()).get();
            Elements elements = htmlDoc.getElementsByAttribute("data-url");
            URL url;
            for (Integer integer : indexesForDownload) {
                try {
                    url = getUrlForDownload(elements.get(integer));
                    new Thread(new MDRunnable(new MusicDownloader(url,
                            fileNames.get(integer)))).start();
                } catch (IndexOutOfBoundsException e) {
                    System.out.printf(INDEX_OUT_OF_BOUNDS, integer + 1);
                }
            }
        } catch (IOException e) {
            System.out.println(FAIL_TO_CONNECTION);
        }
    }

    /**
     * Метод, который ищет ссылку на скачивние в html документе
     *
     * @param el В нём находится html тег
     * @return возвращает ссылку
     * @throws MalformedURLException исключение выбрасываемое при ошибке с URL адресом
     */
    private URL getUrlForDownload(Element el) throws MalformedURLException {
        return new URL(el.attr("data-url"));
    }
}

