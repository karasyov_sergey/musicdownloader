package ru.kzi.downloader;

import java.net.URL;

/**
 * Абстрактный класс Downloader
 *
 * @author KZI 17ИТ17
 */
@SuppressWarnings("unused")
public abstract class Downloader {
    private URL url;

    Downloader(URL url) {
        this.url = url;

    }

    URL getUrl() {
        return url;
    }

    public abstract void download();
}
